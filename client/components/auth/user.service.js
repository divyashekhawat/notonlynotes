'use strict';

angular.module('Journalism')
  .factory('User', function ($resource, apiDomain) {

    var rootpath = apiDomain;
    return $resource(rootpath+'/api/users/:id/:controller', {
      id: '@_id'
    },
    {
      changePassword: {
        method: 'PUT',
        params: {
          controller:'password' 
        }
      },
      get: {
        method: 'GET', 
        params: {
          id:'me'
        }
      },
      getUsers: {
        method: 'GET', 
      }, 
      updateUser:{
          method: 'PUT',
       }
  });
  })
  .factory('ResetPassword',function($resource,apiDomain){
      var rootpath = apiDomain;//"http://192.168.0.19:9000/";//apiDomain;
    var obj = {};
    obj = $resource(rootpath+'/api/users/resetPassword', null, {'update': { method:'PUT' } });
    return obj;
  })
  .factory('ForgotPassword',function($resource,apiDomain){
    var rootpath = apiDomain;
    var obj = {};
     obj = $resource(rootpath+'/api/users/forgotPassword', null, {'update': { method:'PUT' } });
     return obj;
  })
  .factory('Profile',function($resource,apiDomain){
     var rootpath = apiDomain;
     var obj = {};
     obj = $resource(rootpath+'/api/users/:id',null,{'update':{method : 'PUT'}});
     return obj;
  })
 .factory('contactUs', function ($resource, apiDomain) {

    var rootpath = apiDomain;
    return $resource(rootpath+'/api/contactus/:id/:controller', {
      id: '@_id'
    })
    });
    

