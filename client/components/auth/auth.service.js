'use strict';

angular.module('Journalism')
        .value('redirectToUrlAfterLogin', {url: '/'})
        .factory('Auth', function Auth($location, apiDomain, $rootScope, $http, $log, User, $cookieStore, $q, redirectToUrlAfterLogin, $state) {
            var rootpath = apiDomain;
            var currentUser = {};
            if ($cookieStore.get('token')) {
                currentUser = User.get();
            }

            return {
                /*********************** Authenticate user and save token ****************************************/

                login: function (user, successCallback, errorCallback) {
                    var deferred = $q.defer();
                    $http.post(rootpath + '/auth/local', {
                        email: user.email,
                        password: user.password
                    })
                            .then(function(res) {
                                $cookieStore.put('token', res.data.token);
                                User.get(function (res) {
                                  currentUser = res;
                                }, function (error) {
                                   currentUser = error;
                                });
                                deferred.resolve(res.data);
                                successCallback(res.data);
                            }).catch(function(err) {
                              this.logout();
                                deferred.reject(err);
                                errorCallback(err);
                            }.bind(this));
                    return deferred.promise;
                },
                /************************ Delete access token and user info ***********************************/
                logout: function () {
                    $cookieStore.remove('token');
                    currentUser = {};
                   $state.go('main');
                },
                
                saveAttemptUrl: function () {
//                     if($location.path().toLowerCase() !== '/login' || $location.path().toLowerCase() !== '/signup') {
//                     redirectToUrlAfterLogin.url = $location.path();
//                     }
//                     else {
                    //  redirectToUrlAfterLogin.url = '/';
                    //}
                },
                redirectToAttemptedUrl: function () {
                    $location.path(redirectToUrlAfterLogin.url);
                },
                /***************************** Create a new user **********************************************/

                createUser: function (user, callback) {
                    var cb = callback || angular.noop;
                    /*User.get(function(data){
                     var x = data;
                     })*/
                    return User.save(user,
                            function (data) {
                                $cookieStore.put('token', data.token);
                                currentUser = User.get();
                                return cb(user);
                            },
                            function (err) {
                                this.logout();
                                return cb(err);
                            }.bind(this)).$promise;
                },
               
                updateUser: function (user, callback) {
                    var cb = callback || angular.noop;
                    return User.updateUser({id : user._id}, user,
                            function (data) {
                                return cb(data);
                            },
                            function (err) {
                                return cb(err);
                            }.bind(this)).$promise;
                },
                 
                /*** Change password ** @param {String}oldPassword* @param  {String}   newPassword * @param  {Function} callback- optional* @return {Promise}*/

                changePassword: function (oldPassword, newPassword, callback) {
                    var cb = callback || angular.noop;

                    return User.changePassword({id: currentUser._id}, {
                        oldPassword: oldPassword,
                        newPassword: newPassword
                    }, function (user) {
                        return cb(user);
                    }, function (err) {
                        return cb(err);
                    }).$promise;
                },
                /************** Gets all available info on authenticated user** @return {Object} user*********************/

                getCurrentUser: function () {
                    return currentUser;
                },
                /****************** Check if a user is logged in** @return {Boolean}*************************************/
                isLoggedIn: function () {
                    return currentUser.hasOwnProperty('role');
                },
                /************************* Waits for currentUser to resolve before checking if user is logged in*********/

                isLoggedInAsync: function (cb) {
                    if (currentUser.hasOwnProperty('$promise')) {
                        currentUser.$promise.then(function () {
                            cb(true);
                        }).catch(function () {
                            cb(false);
                        });
                    } else if (currentUser.hasOwnProperty('role')) {
                        cb(true);
                    } else {
                        cb(false);
                    }
                },
                /***************************** Check if a user is an admin ** @return {Boolean}*********************/
                isAdmin: function () {
                    return currentUser.role === 'admin';
                },
                /*************************************************** Get auth token*********************************/
                getToken: function () {
                    return $cookieStore.get('token');
                }
            };
        });
