
'use strict';

angular.module('Journalism')
  .factory('socket', function(socketFactory) {

    /*************socket.io now auto-configures its connection when we commit a connection url *******/
    var ioSocket = io('', {
      path: '/socket.io-client'
    });

    var socket = socketFactory({
      ioSocket: ioSocket
    });

    return {
      socket: socket,
      

    /*** Register listeners to sync an array with updates on a model** Takes the array we want to sync, the model name that socket updates are sent from,* and an optional callback function after new items are updated.** @param {String} modelName* @param {Array} array* @param {Function} cb*/
     
      syncUpdates: function (modelName, array, cb) {
        cb = cb || angular.noop;

    /****************** Syncs item creation/updates on 'model:save' *****************************/

        socket.on(modelName + ':save', function (entry) {
          var oldEntry = _.find(array, {_id: entry._id});
          var index = array.indexOf(oldEntry);
          var event = 'created';

          // replace oldEntry if it exists
          // otherwise just add entry to the collection
          if (oldItem) {
            array.splice(index, 1, entry);
            event = 'updated';
          } else {
            array.push(entry);
          }

          cb(event, entry, array);
        });

    /************************** Syncs removed items on 'model:remove' *****************************/
        
        socket.on(modelName + ':remove', function (entry) {
          var event = 'deleted';
          _.remove(array, {_id: entry._id});
          cb(event, entry, array);
        });
      },

    /************** Removes listeners for a models updates on the socket** @param modelName*********/

      unsyncUpdates: function (modelName) {
        socket.removeAllListeners(modelName + ':save');
        socket.removeAllListeners(modelName + ':remove');
      }
    };
  });
