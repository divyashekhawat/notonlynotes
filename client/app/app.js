'use strict';

angular.module('Journalism', [
    'ngCookies',
    'ngResource',
    'ngAnimate',
    'toastr',
    'ngSanitize',
    'btford.socket-io',
    'ui.router',
    'oc.lazyLoad',
    'ui.bootstrap',
    'darthwade.dwLoading',
    'colorpicker.module',
    'nya.bootstrap.select',
])
        .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
            $urlRouterProvider
                    .otherwise('/');
            $locationProvider.html5Mode(true);
            $httpProvider.interceptors.push('authInterceptor');
        })

        //    .constant('apiDomain', 'http://139.59.15.146:7000')
        .constant('apiDomain', 'http://localhost:7000')

        // .constant('apiDomain', '')

        .factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) {
            return {
                /************************* Add authorization token to headers *********************************/

                request: function (config) {
                    config.headers = config.headers || {};
                    if ($cookieStore.get('token')) {
                        config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
                    }
                    return config;
                },
                /************************* Intercept 401s and redirect you to login ***************************/

                responseError: function (response) {
                    if (response.status === 401) {
                        $location.path('/');
                        // remove any stale tokens
                        $cookieStore.remove('token');
                        return $q.reject(response);
                    } else {
                        return $q.reject(response);
                    }
                }
            };
        })
        .run(function ($rootScope, Auth, $state, $stateParams) {

            /********************* Redirect to login if route requires auth and you're not logged in ********/

            $rootScope.$on('$stateChangeStart', function (event, next) {
                Auth.isLoggedInAsync(function (loggedIn) {
                    if (next.authenticate && !loggedIn) {
                        event.preventDefault();
                        Auth.saveAttemptUrl();
                        $state.go('main');
                    }
                });
            });

            $rootScope.$on('$stateChangeSuccess', function (evt, toState) {
                window.document.title = toState.title + ' - Notes';
            });

            $rootScope.spinner = {
                active: false,
                on: function () {
                    this.active = true;
                },
                off: function () {
                    this.active = false;
                }
            };
        });


