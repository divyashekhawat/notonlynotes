'use strict';
angular.module('Journalism')

     .filter('pagination', function ()
        {
            return function (input, start)
            {
                if (!input || !input.length) {
                    return;
                }
                start = +start;
                return input.slice(start);
            };
        })
         .filter('moment', [
            function () {
                return function (date, method) {
                    var momented = moment(date);
                    return momented[method].apply(momented, Array.prototype.slice.call(arguments, 2));
                };
            }
        ])
           
   .filter('capitalize', function () {
            return function (input) {
                if (input !== null) {
                    return input.replace(/\w\S*/g, function (txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
                }
            }
        });