'use strict';

angular.module('Journalism')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        title: 'Journalism',
        url: "/",
        templateUrl: "app/main/main.html",
         controller: 'MainCtrl',
          controllerAs: 'MainC',
             authenticate: false,
          params: {
          sort: null
        },
        resolve: {
            loadPlugin: function ($ocLazyLoad) {
                return $ocLazyLoad.load([
                    {
                        name: 'MainC',
                        files: ['app/main/main.controller.js']
                    }
                ]);
            }
        }
    })
  });