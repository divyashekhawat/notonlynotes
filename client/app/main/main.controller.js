'use strict';

angular.module('Journalism')
        .controller('MainCtrl', function ($scope, Auth, $window, toastr, User, ForgotPassword, $loading, journalMain, Subscription, $state, NotificationService) {
            // var dashboardC = this;
            $scope.init = function () {
                if (Auth.getCurrentUser())
                    $scope.currentUserInfo = Auth.getCurrentUser();
                $scope.getSubscriptionList();
            };
            
                $scope.errors = {};
            $scope.user = {
                first_name: '',
                last_name: '',
                username: '',
                email: '',
                password: ''};
            
               $scope.contactUser = {
                firstName: '',
                lastName: '',
                emailAddress: '',
                message: ''};
              $scope.credentials = {
                email: '',
                password: ''};
           

            /*---------------------open login SIGNUP popup-----------------*/
            $scope.loginSignupPopup = "";
            $scope.currentUserInfo = "";
            
            $scope.showDiv = function (value) {
                $scope.loginSignupPopup = value;
            };
            $scope.loginSignup = function (value) {
                $scope.sub = false;
                $scope.submitted = false;
                if (value == 'signup') {
                    angular.element(document.querySelector("#login_signUp")).addClass("open_layer");
                    $scope.loginSignupPopup = "signup";
                } else {
                    angular.element(document.querySelector("#login_signUp")).addClass("open_layer");
                    $scope.loginSignupPopup = "login";
                }
            };
            
            /*-----------------------SIGNUP POPUP----------------------------*/
                 $scope.signup = function (form1) {
                $scope.submitted = true;
                if (form1.$valid) {
                    $loading.start('signup_loading');
                    Auth.createUser({
                        first_name: $scope.user.first_name,
                        last_name: $scope.user.last_name,
                        username: $scope.user.username,
                        email: $scope.user.email,
                        password: $scope.user.password,
                        role: 'user'
                    })
                            .then(function () {
                                angular.element(document.querySelector("#login_signUp")).removeClass("open_layer");
                                angular.element(document.querySelector("#success_popup")).addClass("open_layer");
                                NotificationService.addNotification({message: 'New User added in Journalism', type: 'newuser'}, function (response) {
                                    //console.log(response);
                                })
                                $scope.successMsg = "You have Successfully Registered";
                                $scope.user = null;
                                $loading.finish('signup_loading');
                                $scope.submitted = false;
                            })
                            .catch(function (err) {
                                err = err.data;
                                $scope.errors = {};
                                $loading.finish('signup_loading');
                                /*********************Update validity of form fields that match the mongoose errors ******************************/

                                if (err) {
                                    angular.forEach(err.errors, function (error, field) {
                                        form1[field].$setValidity('mongoose', false);
                                        $scope.errors[field] = error.message;
                                    });
                                }
                            });
                }
            };
            
            /*-------------------------------lOGIN POPUP-------------------------------*/
               $scope.SignIn = function (form) {
                $scope.sub = true;
                if (form.$valid) {
                    Auth.login({
                        email: $scope.credentials.username,
                        password: $scope.credentials.password
                    }, function (response) {
                        $scope.credentials = null;
                        $scope.loginSignupPopup = "";
                        $scope.sub = false;
                        angular.element(document.querySelector("#login_signUp")).removeClass('open_layer');
                        User.get(function (res) {
                            $scope.currentUserInfo = res;
                            if ($scope.currentUserInfo.role == 'admin')
                                $state.go('admin.dashboard');
                            else if ($scope.currentUserInfo.role == 'user')
                                $state.go('journal.new');
                        }, function (error) {
                            $scope.currentUserInfo = error;
                        });

                    }, function (err) {
                        $scope.errors.other = err.data.message;
                    });
                }
            };

            $scope.loginOauth = function (provider) {
                $window.location.href = '/journal/' + provider;
            };
            
             /*---------------------CLOSE login SIGNUP popup-----------------*/
             $scope.closePopup = function (event) {
                if (event == 'login_signUp')
                    angular.element(document.querySelector("#login_signUp")).removeClass("open_layer");
                if (event == 'forget_pswd')
                    angular.element(document.querySelector("#forget_pswd")).removeClass("open_layer");
                if (event == 'forget_pswd_confirm')
                    angular.element(document.querySelector("#forget_pswd_confirm")).removeClass("open_layer");
                if (event == 'success_popup')
                    angular.element(document.querySelector("#success_popup")).removeClass("open_layer");
            };
            
            /*-------------------FORGOT PASSWORD POPUP-------------------------*/
              $scope.forgotPwd = function () {
                $scope.submit = false;
                angular.element(document.querySelector("#login_signUp")).removeClass('open_layer');
                angular.element(document.querySelector("#forget_pswd")).addClass('open_layer');
            };
           
           
            /******************************* User request to Forgot Password ***************************************/

            $scope.email = {};
            $scope.forgot = function (form) {
                $scope.submit = true;
                $scope.errors.other = '';
                if ($scope.email != "") {
                    if (form.$valid) {
                        ForgotPassword.save({email: $scope.Userforgot.email}, function (res) {
                            if (res.authentication) {
                                angular.element(document.querySelector("#forget_pswd")).removeClass("open_layer");
                                angular.element(document.querySelector("#forget_pswd_confirm")).addClass("open_layer");
                            } else
                                $scope.errors.other = res.message;
                        });
                    }
                }
            };
            
           /*-----------------------FORGOT PASSWORD CONFIRMATION POPUP------------------------*/
            $scope.confirmPopup = function () {
                angular.element(document.querySelector("#forget_pswd")).removeClass('open_layer');
                angular.element(document.querySelector("#forget_pswd_confirm")).addClass('open_layer');
            };
            
            /*--------------------- REQUEST FOR SUBSCRIPTION lIST-----------------*/
            $scope.getSubscriptionList = function () {
                Subscription.get().$promise.then(function (response) {
                    $scope.subscriptionList = response.data;
                }, function (error) {
                    console.log(error);
                });

            };

             /*--------------------------SLIDER-------------------------------*/
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            // var slides = $scope.slides = [];
            var currIndex = 0;
            var slides = $scope.slides = [
                {
                    "image": "../../assests/images/banner1.jpg",
                    "title": "A Journaling Service for",
                    "text": "capturing  your ideas",
                    "id": 0
                },
                {
                    "image": "../../assests/images/banner1.jpg",
                    "title": "A Journaling Service for",
                    "id": 1
                },
                {
                    "image": "../../assests/images/banner1.jpg",
                    "title": "A Journaling Service for",
                    "id": 2
                },
                {
                    "image": "../../assests/images/banner1.jpg",
                    "title": "A Journaling Service for",
                    "id": 3
                }
            ]

            // Randomize logic below

            function assignNewIndexesToSlides(indexes) {
                for (var i = 0, l = slides.length; i < l; i++) {
                    slides[i].id = indexes.pop();
                }
            }

            function generateIndexesArray() {
                var indexes = [];
                for (var i = 0; i < currIndex; ++i) {
                    indexes[i] = i;
                }
                return shuffle(indexes);
            }

            // http://stackoverflow.com/questions/962802#962890
            function shuffle(array) {
                var tmp, current, top = array.length;

                if (top) {
                    while (--top) {
                        current = Math.floor(Math.random() * (top + 1));
                        tmp = array[current];
                        array[current] = array[top];
                        array[top] = tmp;
                    }
                }
                return array;
            }
        
            /*------------------------CONTACT US-----------------------*/
             $scope.contactUs = function (form1) {
                $scope.submitted = true;
                if (form1.$valid) {
                    console.log($scope.contactUser);
                    $loading.start('signup_loading');
                    journalMain.sendMessage({
                        firstName: $scope.contactUser.firstName,
                        lastName: $scope.contactUser.lastName,
                        email: $scope.contactUser.emailAddress,
                        message: $scope.contactUser.message,
                        isUpdated: false
                    })
                            .then(function () {
                                console.log("returd");
                                angular.element(document.querySelector("#login_signUp")).removeClass("open_layer");
                                angular.element(document.querySelector("#success_popup")).addClass("open_layer");
                                $scope.successMsg = "Your form is successfully submitted !";
                                $scope.contactUser = null;
                                $loading.finish('signup_loading');
                                $scope.submitted = false;
                            })
                            .catch(function (err) {
                                err = err.data;
                                $scope.errors = {};
                                $loading.finish('signup_loading');
                            });
                }
            };

            $scope.init();
        });

/******************************* Main controller ends here ***************************************/
