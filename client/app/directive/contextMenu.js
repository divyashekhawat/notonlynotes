////////////////////////Custom Right Click event////////////////////////

'use strict';
angular.module('Journalism')
        .directive( "contextMenu", function($compile){
    var contextMenu = {};
    contextMenu.restrict = "AE";
    contextMenu.link = function( lScope, lElem, lAttr ){
        lElem.on("contextmenu", function (e) {
            e.preventDefault(); // default context menu is disabled
            //  The customized context menu is defined in the main controller. To function the ng-click functions the, contextmenu HTML should be compiled.
            if(!document.getElementById("contextmenu-node"))
            lElem.append( $compile( lScope[ lAttr.contextMenu ])(lScope) );
            // The location of the context menu is defined on the click position and the click position is catched by the right click event.
             angular.element(document.querySelector("#contextmenu-node")).css('top', e.clientY +'px');
             angular.element(document.querySelector("#contextmenu-node")).css('left', e.clientX +'px');
        });
        lElem.on("mouseleave", function(e){
            console.log("Leaved the div");
            // on mouse leave, the context menu is removed.
            if( document.getElementById("contextmenu-node"))
                 document.getElementById("contextmenu-node").remove();
             
        });
    };
    return contextMenu;
});