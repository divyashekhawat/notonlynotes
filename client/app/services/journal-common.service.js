angular.module('Journalism')
        .service("journalCommonService", function () {

 var subscriptionDetail= {};
 var  selectedDate = null;
 
    this.setSubscription = function (data) {
        subscriptionDetail = data;
    };

    this.getSubscription = function () {
        return subscriptionDetail;
    };
    
    this.setSelectedDate = function (data){
      selectedDate = data;  
    };
    
    this.getSelectedDate = function(){
      return selectedDate;  
    };
   
});



