(function () {
    'use strict';
    angular

            .module('journal', ['mwl.calendar', 'ngAnimate', 'ui.bootstrap', 'colorpicker.module'])
            .config(['calendarConfig', function (calendarConfig) {
                    calendarConfig.allDateFormats.angular.title.day = 'EEE d MMM, yyyy';
                    calendarConfig.allDateFormats.angular.date.weekDay = 'EEE';
                    calendarConfig.allDateFormats.moment.title.day = 'ddd D MMM';
                    calendarConfig.allDateFormats.moment.date.weekDay = 'ddd';
                    calendarConfig.i18nStrings.weekNumber = 'Week {week}';
                }])
            .service('number', function () {
                return {
                    isPositive: function (operationPrice) {
                        return String(operationPrice).indexOf("-") == -1;
                    }
                };
            })
            .controller('journalController', function ($scope, $rootScope, moment, alert, calendarConfig, Auth, TopicService, journalCommonService) {
                $scope.otherError = false;
                $scope.init = function () {
                    if (Auth.getCurrentUser())
                        $scope.currentUserInfo = Auth.getCurrentUser();
                    $scope.currentDate = new Date();
                    $scope.selectedCalDate = moment($scope.currentDate).format('YYYY-MM-DD');
                    journalCommonService.setSelectedDate($scope.selectedCalDate);
                    $scope.getTopic($scope.selectedCalDate);
                };

                /*---------------------calendar start-------------------*/
                var vm = this;
                $scope.treedata = [];
                $scope.notificationActive = false;
                //These variables MUST be set as a minimum for the calendar to work
                vm.calendarView = 'month';
                //vm.weekDays =//
                vm.viewDate = new Date();
                var actions = [{
                        label: '<i class=\'glyphicon glyphicon-pencil\'></i>',
                        onClick: function (args) {
                            alert.show('Edited', args.calendarEvent);
                        }
                    }, {
                        label: '<i class=\'glyphicon glyphicon-remove\'></i>',
                        onClick: function (args) {
                            alert.show('Deleted', args.calendarEvent);
                        }
                    }];

                vm.addEvent = function (e) {
                    $scope.otherError = false;
                    $scope.topic = {};
                    if (e == 1)
                        angular.element(document.querySelector("#topic_setting")).addClass('open_layer');
                    else if (e == 0)
                        angular.element(document.querySelector("#topic_setting")).removeClass('open_layer');
                };

                vm.eventClicked = function (event) {
                    console.log("event" + event);
                    alert.show('Clicked', event);
                };

                vm.modifyCell = function (event) {
                    console.log("modifyCell" + event);
                };

                vm.eventEdited = function (event) {
                    alert.show('Edited', event);
                };

                vm.eventDeleted = function (event) {
                    alert.show('Deleted', event);
                };

                vm.eventTimesChanged = function (event) {
                    alert.show('Dropped or resized', event);
                };

                vm.toggle = function ($event, field, event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    event[field] = !event[field];
                };
                
                /*----------------Function called on calender date clicked -------------------*/
                vm.timespanClicked = function (date, cell) {
                    $rootScope.isTopicEditor = false;
                    $scope.selectedCalDate = moment(date).format('YYYY-MM-DD');
                    journalCommonService.setSelectedDate($scope.selectedCalDate);
                    $scope.getTopic($scope.selectedCalDate);
                    $rootScope.loadEditor();
                };

                 /*----------------Get  topic list -------------------*/
                $scope.getTopic = function (curDate) {
                    TopicService.get({date: curDate}).$promise.then(function (response) {
                        $rootScope.topicList = response.data;
                        $scope.nodeData = $rootScope.topicList;
                        if ($scope.nodeData.length > 0) {
                            for (var i = 0; i < $scope.nodeData.length; i++) {
                                $scope.nodeData[i].collapsed = true;
                            }
                        }
                    }, function (error) {
                        console.log(error);
                    });
                };
                
                /*----------------------function clicked on topic and entity click-------------------*/
                $scope.openNode = function (type, topicData, entityData) {
                    $rootScope.isTopicEditor = true;
                    var journalScope = angular.element(document.getElementById('new-journal-editor')).scope();
                    if (type == 'topic') {
                        topicData.collapsed = !topicData.collapsed;
                        journalScope.journal.journalContent = '';
                        if (topicData.entities.length > 0) {
                            for (var i = 0; i < topicData.entities.length; i++) {
                                journalScope.journal.journalContent = journalScope.journal.journalContent + topicData.entities[i].content;
                            }
                        }
                    } else if (type == 'entity') {
                        journalScope.journal.journalContent = '';
                        for (var i = 0; i < topicData.entities.length; i++) {
                            if (topicData.entities[i]._id == entityData._id) {
                                journalScope.journal.journalContent = journalScope.journal.journalContent + '<span style=\"background-color: rgb(115, 130, 193);\">' + topicData.entities[i].content + '</span>';
                            } else {
                                journalScope.journal.journalContent = journalScope.journal.journalContent + topicData.entities[i].content;
                            }
                        }
                    }

                };
                
                /*-------------------------show hide side bar--------------------*/
                $scope.open = false;
                $scope.openCloseNav = function () {
                    if (!$scope.open) {
                        document.getElementById("mySidenav").style.width = "315px";
                        document.getElementById("main").style.marginLeft = "315px";
                        $scope.open = true;
                    } else {
                        document.getElementById("mySidenav").style.width = "0";
                        document.getElementById("main").style.marginLeft = "0";
                        $scope.open = false;
                    }
                }

                /*-------------Display topic status---------------------*/
                $scope.topicStatus = function (data) {
                    angular.element(document.querySelector("#goal_statusPop")).addClass('open_layer');
                    $scope.goalStatus = data;
                };

                /*-------------------Delete topic--------------------*/
                $scope.deleteTopic = function (topicId) {
                    TopicService.delete({id: topicId}).$promise.then(function (response) {
                        console.log(response);
                    }, function (error) {
                        console.log(error);
                    });
                };
                
                 /*-------------Hide topic status popup---------------------*/
                $scope.closeTopicStatus = function () {
                    angular.element(document.querySelector("#goal_statusPop")).removeClass('open_layer');
                };

                $scope.dateOptions = {
                    minDate: new Date(),
                    dateFormat: 'mm-dd-yy',
                };

                $scope.init();
                $scope.logoutJournal = function () {
                    Auth.logout();
                };
                
                /*---------------------Add Topic-------------------*/
                $scope.topicFormSubmit = function (form) {
                    $scope.topicSub = true;
                    if (form.$valid) {
                        if ($scope.topic.goals.isSet && $scope.topic.setting.isSet && ($scope.topic.goals.wordCount > $scope.topic.setting.overAllWordCount)) {
                            $scope.otherError = true;
                            $scope.otherErrorMessage = "Overroll Word count should be grater then to Word count!"
                        } else {
                            TopicService.create($scope.topic).$promise.then(function (res) {
                                $scope.topicSub = false;
                                $scope.otherError = false;
                                angular.element(document.querySelector("#topic_setting")).removeClass('open_layer');
                            }, function (error) {
                                $scope.errors.other = error.message;
                            });
                        }
                    }
                };


                /*---------------------Calendar Code End-------------------*/

                $scope.$watch('abc.currentNode', function (newObj, oldObj) {
                    if ($scope.abc && angular.isObject($scope.abc.currentNode)) {
                        console.log('Node Selected!!');
                        console.log($scope.abc.currentNode);
                    }
                }, false);
                $scope.open1 = function () {
                    $scope.popup1.opened = true;
                };

                $scope.popup1 = {
                    opened: false
                };
                $scope.open2 = function () {
                    $scope.popup2.opened = true;
                };

                $scope.popup2 = {
                    opened: false
                };

                $scope.showNotification = function () {
                    $scope.notificationActive = !$scope.notificationActive;
                };

                $scope.selectedNode = function (node) {
                    console.log(node);
                };

            })
            .factory('alert', function ($uibModal) {
                function show(action, event) {
                    return $uibModal.open({
                        templateUrl: 'calendar/modelContent.html',
                        controller: function () {
                            var vm = this;
                            vm.action = action;
                            vm.event = event;
                        },
                        controllerAs: 'vm'
                    });
                }

                return {
                    show: show
                };

            });

})();

