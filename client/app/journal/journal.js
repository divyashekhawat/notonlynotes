'use strict';

angular.module('Journalism')
        .config(function ($stateProvider) {
            $stateProvider
                    .state('journal', {
                        title: 'Journalism',
                        url: "/journal",
                        templateUrl: "app/journal/journal.html",
                        controller: 'journalController',
                        controllerAs: 'journalC',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'journalC',
                                        files: ['app/journal/journal.controller.js']
                                    }
                                ]);
                            }
                        }
                    })
                    .state('journal.new', {
                        title: 'New Journal',
                        url: "/new",
                        templateUrl: "app/journal/new-journal/journal.new.html",
                        controller: 'newjournalCtrl',
                        controllerAs: 'newjrnlC',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                  {
                                        name: 'newjrnlC',
                                        files: ['app/journal/new-journal/journal.new.controller.js']
                                    }
                                ]);
                            }
                        }
                    })
                    .state('journal.subscription', {
                        url: "/subscription-plan",
                        templateUrl: "app/journal/subscription-plan/journal.subscription.html",
                        controller: 'subscriptionCtrl',
                        controllerAs: 'subscriptionC',
                        title: 'Sunscription Plan',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'subscriptionC',
                                        files: ['app/journal/subscription-plan/journal.subscription.controller.js']
                                    }
                                ]);
                            }
                        }

                    })
                    .state('journal.setting', {
                        url: "/setting",
                        templateUrl: "app/journal/setting/journal.setting.html",
                        controller: 'settingCtrl',
                        controllerAs: 'settingC',
                        title: 'Setting',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'settingC',
                                        files: ['app/journal/setting/journal.setting.controller.js']
                                    }
                                ]);
                            }
                        }

                    })
                    .state('journal.plans', {
                        url: "/plans-bill",
                        templateUrl: "app/journal/plans/journal.plan.html",
                        controller: 'planCtrl',
                        controllerAs: 'planC',
                        title: 'Plans Bill',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'planC',
                                        files: ['app/journal/plans/journal.plan.controller.js']
                                    }
                                ]);
                            }
                        }

                    }).state('journal.plandetails', {
                title: 'Plan Details',
                url: "/plandetails/:plan",
                templateUrl: "app/journal/plan-detail/journal.plandetails.html",
                controller: 'plandetailsCtrl',
                controllerAs: 'planDetailsC',
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'planDetailsC',
                                files: ['app/journal/plan-detail/journal.plandetails.controller.js']
                            }
                        ]);
                    }
                }

            })
        });