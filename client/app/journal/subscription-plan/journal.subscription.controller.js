(function () {
    'use strict';
    angular
            .module('subscription', [])

            .controller('subscriptionCtrl', function ($scope, $rootScope, $state, Subscription, journalCommonService) {
                $scope.init = function () {
                    $scope.getSubscriptionList();
                };
                  $rootScope.isCalender = false;
                $scope.planedesc = "";
                /*------------------subscription plan detail page redirection------------------*/
                $scope.subsPlan = function (value) {
                    $scope.plan = value;
                    if ($scope.plan.name == 'Pro') {
                        angular.element(document.querySelector("#plane_details")).addClass('open_layer');
                    } else {
                        journalCommonService.setSubscription($scope.plan);
                        $state.go('journal.plandetails', {plan: $scope.plan.name});
                    }
                };
                
                /*-------------------get subscription plane List --------------------*/
                $scope.getSubscriptionList = function () {
                     Subscription.get().$promise.then(function (response) {
                     $scope.subscription = response.data;
                    }, function (error) {
                        console.log(error);
                    });
                   
                }
                $scope.closeForm = function () {
                    angular.element(document.querySelector("#plane_details")).removeClass('open_layer');
                      journalCommonService.setSubscription($scope.plan);
                    $state.go('journal.plandetails', {plan: $scope.plan.name});
                }
                $scope.suggestionForm = function (formData) {
                    console.log(JSON.stringify(formData));
                    angular.element(document.querySelector("#plane_details")).removeClass('open_layer');
                }
                $scope.init();
            })

})();

