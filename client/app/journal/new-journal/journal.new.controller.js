/* global socket */

(function () {
    'use strict';
    angular
            .module('newjournal', ['ui.bootstrap', 'ngQuill'])
            .config(['ngQuillConfigProvider', function (ngQuillConfigProvider) {
                    ngQuillConfigProvider.set({
                        toolbar: ['bold', 'italic', 'underline', 'strike', 'blockquote', 'link', {'size': ['small', false, 'large', 'huge']}, {'header': [1, 2, 3, 4, 5, 6, false]}, {'script': 'sub'}, {'script': 'super'}, {'list': 'ordered'}, {'list': 'bullet'}, {'color': []}, {'background': []}, {'align': []}, 'code-block']
                    }, 'snow', 'custom placeholder', ['bold', 'italic', 'underline', 'strike', 'blockquote', 'link', 'size', 'header', 'script', 'list', 'color', 'background', 'align', 'code-block']);
                }])
            .factory('socket', function (socketFactory) {
                return socketFactory({
                    ioSocket: io.connect('http://localhost:7000')
                });
            })
            .controller('newjournalCtrl', function ($scope, $rootScope, journalCommonService, NewJournal, $timeout, socket, TopicService) {
                $rootScope.isCalender = true;
                $scope.selectedText = '';
               $scope.init = function () {
                    $rootScope.loadEditor();
                };
                
                /*---------------------Socket Event ---------------------------*/
                   socket.on('send:message', function (message) {
                    //  alert(message);
                    $scope.journal.journalContent = message;
                }); 
                
                /*--------------------- Start Quill Editor---------------*/
                $rootScope.loadEditor = function () {
                    $scope.journal = {journalAddDate: journalCommonService.getSelectedDate()}
                    $scope.getJournal($scope.journal.journalAddDate);
                };
             
                $scope.getJournal = function (selectedDate) {
                    NewJournal.getSelectedJournal({journalAddDate: selectedDate}, function (response) {
                        if (response.data) {
                            $scope.journal = response.data;
                        } else {
                            NewJournal.addJournal({journalAddDate: selectedDate}, function (res) {
                                if (!res.error) {
                                    $scope.journal = res;
                                } else {
                                    $scope.errors.other = res.message;
                                }
                            })
                        }
                    })
                };

                  $scope.getSelectedText = function () {
                      var html = "";
                    if (typeof window.getSelection != "undefined") {
                        var sel = window.getSelection();
                        if (sel.rangeCount) {
                            var container = document.createElement("div");
                            for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                                container.appendChild(sel.getRangeAt(i).cloneContents());
                            }
                            html = container.innerHTML;
                        }
                    } else if (typeof document.selection != "undefined") {
                        if (document.selection.type == "Text") {
                            html = document.selection.createRange().htmlText;
                        }
                    }
                    //alert(html);
                    $scope.selectedText = html;
                };
                
                $scope.addEntity = function (data) {
                    TopicService.createEntity({id:data._id, content:$scope.selectedText}).$promise.then(function (response) {
                          $scope.selectedText = '';
                    }, function (error) {
                        console.log(error);
                    });
                     };

              
               //  console.log($scope.journal.journalContent.split(/\s+/).length);
                $scope.editorCreated = function (editor) {
                    console.log(editor);
                };
                $scope.contentChanged = function (editor, html, text) {
                     socket.emit('send:message', $scope.journal.journalContent);
                    NewJournal.updateJournal($scope.journal, function (response) {
                        if (!response.error) {
                            console.log(response)
                        } else {
                            $scope.errors.other = response.message;
                        }
                    })

                };

               
                /*--------------------- End Quill Editor---------------*/
                $scope.init();

            })

})();
