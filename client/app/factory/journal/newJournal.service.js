'use strict';

angular.module('Journalism')
        .factory('NewJournal', function NewJournal(Journal) {

            return {
                /*********************** MANAGE NEW JOURNAL ****************************************/

                addJournal: function (journalDetail, callback) {
                    return Journal.save(journalDetail, function (data) {
                       return callback(data);
                    },
                        function (err) {
                                return callback(err);
                            }.bind(this)).$promise;
                },
                
                  /******************************* GET JOURNAL ****************************************/

                getSelectedJournal: function (journalDate, callback) {
                    return Journal.get(journalDate, function (data) {
                       return callback(data);
                    },
                        function (err) {
                                return callback(err);
                            }.bind(this)).$promise;
                },
                
                /******************************** UPDATE JOURNAL ********************************/
                
                 updateJournal: function (journalDetail, callback) {
                    return Journal.update({id : journalDetail._id}, journalDetail , function (data) {
                        return callback(data);
                    },
                            function (err) {
                                return callback(err);
                            }.bind(this)).$promise;
                },
            };
        });
