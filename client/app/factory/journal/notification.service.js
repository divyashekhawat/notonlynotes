'use strict';

angular.module('Journalism')
        .factory('NotificationService', function NotificationService(Notification) {

            return {
                /*********************** Notification Setting ****************************************/

                getNotifications: function (callback) {
                    return Notification.get(function (data) {
                        return callback(data);
                    },
                            function (err) {
                                return callback(err);
                            }.bind(this)).$promise;

                },
                addNotification: function (notification, callback) {
                    return Notification.save(notification, function (data) {
                        return callback(data);
                    },
                            function (err) {
                                return callback(err);
                            }.bind(this)).$promise;

                },
                updateNotification: function (callback) {
                    return Notification.update(function (data) {
                        return callback(data);
                    },
                            function (err) {
                                return callback(err);
                            }.bind(this)).$promise;

                },
                  deleteNotification: function (notificationId, callback) {
                    return Notification.delete({id: notificationId}, function (res) {
                        return callback(res);
                    }, function (err) {
                        return callback(err);
                    }.bind(this)).$promise;

                }
            };
        });
