'use strict';

angular.module('Journalism')
        .factory('Users', function Users(User, $q) {
            return {
                getUsersData: function (callback) {
                    var cb = callback || angular.noop;
                    var deferred = $q.defer();
                    return User.getUsers(function (res) {
                        deferred.resolve(res.data);
                        cb(res.data);
                    },
                            function (err) {
                                deferred.resolve(err);
                                cb(err);
                            }.bind(this)).$promise;
                    return deferred.promise;
                },
            };
        });
