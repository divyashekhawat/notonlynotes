'use strict';

angular.module('Journalism')
        .factory('Setting', function Setting(apiDomain, $rootScope, $http, $q) {
            var rootpath = apiDomain;
     
            return {
                /*********************** Account Setting ****************************************/

                accountSetting: function (userData, params, callback) {
                    var cb = callback || angular.noop;
                    var deferred = $q.defer();
                    $http.post(rootpath + '/api/users/' +params, userData)
                            .then(function successCallback(res) {
                               deferred.resolve(res.data);
                                callback(res.data);
                            }, function errorCallback(err) {
                               deferred.reject(err);
                                callback(err);
                            }.bind(this));
                    return deferred.promise;
                }   
            };
        });
