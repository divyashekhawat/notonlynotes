'use strict';

angular.module('Journalism')
        .factory('Subscription', function ($resource, apiDomain) {
             var rootpath = apiDomain;
            return $resource(rootpath + '/api/subscription/:id/:controller', {
                id: '@_id'
            },
                    {
                        get: {
                            method: 'GET',
                            params: {
                                controller: 'all'
                            }
                        },
                          create:{
                             method: "POST",
                        },
                        delete: {
                            method: 'DELETE'
                        },
                        update: {
                            method: 'PUT',
                        }
                    });
        })
         .factory('Coupon', function ($resource, apiDomain) {

            var rootpath = apiDomain;
            return $resource(rootpath + '/api/coupon/:id/:controller', {
                id: '@_id'
            },
                    {
                        get: {
                            method: 'GET',
                               params: {
                                controller: 'all'
                            }
                        },
                        create:{
                             method: "POST",
                        },
                        delete: {
                            method: 'DELETE'
                        },
                        update: {
                            method: 'PUT',
                        }
                    });
        })
        .factory('Journal', function ($resource, apiDomain) {
            var rootpath = apiDomain;
            return $resource(rootpath + '/api/journal/:id/:controller', {
                id: '@_id'
            },
                    {
                        get: {
                            method: 'POST',
                            params: {
                                controller: 'one'
                            }
                         },
                         update: {
                            method: 'PUT',
                        }
                    });
        })
        .factory('Notification', function ($resource, apiDomain) {
            var rootpath = apiDomain;
            return $resource(rootpath + '/api/notification/:id/:controller', {
                id: '@_id'
            },
                    {
                        get: {
                            method: 'GET',
                            params: {
                                controller: 'all'
                            }
                        },
                        'create': {
                            method: "POST",
                        },
                        delete: {
                            method: 'DELETE',
                            params: {
                              id:'@id'
                            }
                        },
                        update: {
                            method: 'PUT',
                        }
                    });
        })
          .factory('StaticContentService', function ($resource, apiDomain) {
            var rootpath = apiDomain;
            return $resource(rootpath + '/api/content/:id/:type', {
                id: '@_id'
            },
                    {
                        'get': {
                            method: "GET",
                            params: {
                                type: '@type'
                            }
                        },
                         'getAll': {
                            method: "GET",
                           isArray: true,
                        },
                        'create': {
                            method: "POST",
                        },
                        'update': {
                            method: "PUT",
                        }
                    });
        })
        .factory('TopicService', function ($resource, apiDomain) {
            var rootpath = apiDomain;
            return $resource(rootpath + '/api/topic/:controller/:id/:date', {
                id: '@_id'
            },
                    {
                        'get': {
                            method: "GET",
                             params: {
                                date: '@date'
                            }
                          },
                        'create': {
                            method: "POST",
                        },
                        'update': {
                            method: "PUT",
                        },
                        'delete': {
                            method: "DELETE",
                             params: {
                                id: '@id'
                            }
                        },
                        'createEntity': {
                            method: "PUT",
                             params: {
                                controller: 'entity',
                                id: '@id'
                            }
                        },
                    });
        });
