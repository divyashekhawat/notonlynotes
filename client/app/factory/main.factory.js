'use strict';

angular.module('Journalism')
        .factory('journalMain', function journalMain(contactUs) {

            return {
              
                sendMessage: function (messageInfo, callback) {
                    var cb = callback || angular.noop;
                    return contactUs.save(messageInfo,
                            function (data) {
                                return cb(data);
                            },
                            function (err) {
                               return cb(err);
                            }.bind(this)).$promise;
                },
               
            };
        });
