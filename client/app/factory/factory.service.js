'use strict';

angular.module('Journalism')

/****************************************Sample factory (dummy) ************************************************/

  .factory('factory', [function () {
    var somValue = 42;
    
    return {
      someMethod: function () {
        return somValue;
      }
    };
  }])


