'use strict';

angular.module('Journalism')
        .config(function ($stateProvider) {
            $stateProvider
                    .state('admin', {
                        title: 'Admin',
                        url: "/admin",
                        templateUrl: "app/admin-panel/admin.html",
                        controller: 'adminController',
                        controllerAs: 'adminC',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
//                                    {
//                                        serie: true, name: 'angular-sidebar',
//                                        files: ['app/journal/js/angular-ap-lateral-slide-menu.js']
//                                    },
                                    {
                                        name: 'adminC',
                                        files: ['app/admin-panel/admin.controller.js']
                                    }
                                ]);
                            }
                        }
                    })
                    .state('admin.dashboard', {
                        title: 'Dashboard',
                        url: "/dashboard",
                        templateUrl: "app/admin-panel/dashboard/admin.dashboard.html",
                        controller: 'dashboardCtrl',
                        controllerAs: 'dashboardC',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'dashboardC',
                                        files: ['app/admin-panel/dashboard/admin.dashboard.controller.js']
                                    }
                                ]);
                            }
                        }
                    })
                    .state('admin.subadmin', {
                        title: 'Manage Sub Admin',
                        url: "/subadmin/:page",
                        templateUrl: "app/admin-panel/dashboard/sub-admin/admin.managesubadmin.html",
                        controller: 'manageSubAdminCtrl',
                        controllerAs: 'manageSubAdminC',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {serie: true, name: 'checklist-model',
                                        files: ['app/directive/Checklist-model.js']
                                    },
                                    {
                                        name: 'manageSubAdminC',
                                        files: ['app/admin-panel/dashboard/sub-admin/admin.managesubadmin.controller.js']
                                    }
                                ]);
                            }
                        }
                    })
                    .state('admin.subscription', {
                        title: 'Subscription Management',
                        url: "/subscription",
                        templateUrl: "app/admin-panel/subscription-management/admin.subscription.html",
                        controller: 'subscriptionCtrl',
                        controllerAs: 'subscriptionC',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        serie: true, name: 'angular-base64-upload',
                                        files: ['assests/js/angular-base64-upload.js']
                                    },
                                    {
                                        name: 'subscriptionC',
                                        files: ['app/admin-panel/subscription-management/admin.subscription.controller.js']
                                    }
                                ]);
                            }
                        }
                    })
                    .state('admin.coupon', {
                        title: 'Coupon Management',
                        url: "/coupon",
                        templateUrl: "app/admin-panel/coupon-management/admin.coupon.html",
                        controller: 'couponCtrl',
                        controllerAs: 'couponC',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        serie: true, name: 'angular-base64-upload',
                                        files: ['assests/js/angular-base64-upload.js']
                                    },
                                    {
                                        name: 'couponC',
                                        files: ['app/admin-panel/coupon-management/admin.coupon.controller.js']
                                    }
                                ]);
                            }
                        }
                    })
                    .state('admin.notification', {
                        title: 'Notification',
                        url: "/notification",
                        templateUrl: "app/admin-panel/notification/admin.notification.html",
                        controller: 'notificationCtrl',
                        controllerAs: 'notificationC',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'couponC',
                                        files: ['app/admin-panel/notification/admin.notification.controller.js']
                                    }
                                ]);
                            }
                        }
                    })
                    .state('admin.content', {
                        title: 'Content Management',
                        url: "/static-content",
                        templateUrl: "app/admin-panel/static-content/static-content.html",
                        controller: 'contentManagementCtrl',
                        controllerAs: 'contentC',
                        authenticate: true,
                        resolve: {
                            loadPlugin: function ($ocLazyLoad) {
                                return $ocLazyLoad.load([
                                    {
                                        name: 'contentC',
                                        files: ['app/admin-panel/static-content/static-content.controller.js']
                                    }
                                ]);
                            }
                        }
                    })
        });
