(function () {
    'use strict';
    angular
            .module('couponManagement', ['naif.base64'])

            .controller('couponCtrl', function ($scope, Coupon, toastr) {
                $scope.isCouponAddEdit = false;
                $scope.sortType = 'name'; // set the default sort type
                $scope.sortReverse = false;  // set the default sort order
                $scope.searchCoupon = '';
                $scope.init = function () {
                    $scope.getCouponslist();
                };

                $scope.getCouponslist = function () {
                    Coupon.get().$promise.then(function (response) {
                        $scope.couponList = response.data;
                    }, function (error) {
                        console.log(error);
                    });
                };

                $scope.addCoupon = function () {
                    $scope.coupon = {
                        name: '',
                        description: '',
                        discount: '',
                        noOfUser: '',
                        startDate: '',
                        endDate: ''
                    };
                    $scope.isCouponAddEdit = true;

                };

                $scope.editCoupon = function (value) {
                    $scope.coupon = value;
                    $scope.isCouponAddEdit = true;
                };


                $scope.deleteCoupon = function (value) {
                    Coupon.delete({id: value._id}).$promise.then(function (response) {
                      toastr.success("Coupon Deleted", "Successfully!");
                        $scope.getCouponslist();
                    }, function (error) {
                           toastr.error("Something went wrong", "Error!");
                        console.log(error);
                    });
                };

                $scope.open1 = function () {
                    $scope.popup1.opened = true;
                };

                $scope.popup1 = {
                    opened: false
                };
                $scope.open2 = function () {
                    $scope.popup2.opened = true;
                };

                $scope.popup2 = {
                    opened: false
                };

                $scope.saveCoupon = function (form) {
                    $scope.sub = true;
                    if (form.$valid) {
                        if ($scope.coupon._id == null) {
                            Coupon.create($scope.coupon).$promise.then(function (response) {
                                $scope.sub = false;
                                $scope.isCouponAddEdit = false;
                                $scope.getCouponslist();
                                  toastr.success("Coupon Added", "Successfully!");
                            }, function (error) {
                                $scope.errors.other = error.message;
                                  toastr.success("Coupon Updated", "Successfully!");
                            });
                        } else {
                               Coupon.update($scope.coupon).$promise.then(function (response) {
                                $scope.sub = false;
                                $scope.isCouponAddEdit = false;
                                   toastr.success("Coupon Updated", "Successfully!");
                                $scope.getCouponslist();
                            }, function (error) {
                                $scope.errors.other = error.message;
                                  toastr.error("Something went wrong", "Error!");
                            });
                            }
                    }
                };

                $scope.onLoad = function (fileObj) {
                    //   console.log("fileObj" +JSON.stringify(fileObj));
                };
                $scope.init();
                $scope.files = [];

            })

})();

