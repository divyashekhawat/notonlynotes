(function () {
    'use strict';
    angular

            .module('journal', [])
            .controller('adminController', function ($scope, Auth, NotificationService) {
                $scope.notificationActive = false;
                $scope.init = function () {
                    if (Auth.getCurrentUser())
                        $scope.currentUserInfo = Auth.getCurrentUser();
                    $scope.getNotificationList();
                };

                $scope.getNotificationList = function () {
                    NotificationService.getNotifications(function (res) {
                        $scope.notification = res.data;
                        $scope.notificationCount = 0;
                        $scope.unreadNotification = [];
                        for (var i = 0; i < $scope.notification.length; i++) {
                            if ($scope.notification[i].status == 'unread') {
                                $scope.notificationCount++;
                                $scope.unreadNotification.push($scope.notification[i]);
                            }
                        }
                    })
                };
                
                    $scope.open = false;
                $scope.openCloseNav = function () {
                    if (!$scope.open) {
                        document.getElementById("mySidenav-admin").style.width = "315px";
                        document.getElementById("main-admin").style.marginLeft = "315px";
                        $scope.open = true;
                    } else {
                        document.getElementById("mySidenav-admin").style.width = "0";
                        document.getElementById("main-admin").style.marginLeft = "0";
                        $scope.open = false;
                    }
                }
                
                $scope.logoutAdminPanel = function () {
                    Auth.logout();
                }

                $scope.showNotification = function () {
                    $scope.notificationActive = !$scope.notificationActive;
                    if ($scope.unreadNotification.length > 0) {
                        NotificationService.updateNotification(function (res) {
                        });
                    }
                }

                $scope.init();

            })
})();


