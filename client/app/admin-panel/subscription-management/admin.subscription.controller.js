(function () {
    'use strict';
    angular
            .module('subscription', ['naif.base64'])

            .controller('subscriptionCtrl', function ($scope, Subscription, toastr) {
                $scope.isSubscriAddEdit = false;
                 $scope.sortType = 'name'; // set the default sort type
                $scope.sortReverse = false;  // set the default sort order
                $scope.searchSubscription = '';
                $scope.init = function () {
                    $scope.getSubscriptionList();
                };
                
                /*-----------------GET SUBSCRIPTION LIST----------------*/
                $scope.getSubscriptionList = function () {
                      Subscription.get().$promise.then(function (response) {
                      $scope.subscriptionList = response.data;
                    }, function (error) {
                        console.log(error);
                    });
                  };

                   /*-----------------OPen Subsciption----------------*/
                $scope.addSubsciption = function () {
                  $scope.subscription = {
                    name: '',
                    description: '',
                    price: '',
                    validity: ''};
                    $scope.isSubscriAddEdit = true;
                };
                
                  /*-----------------Edit Subsciption----------------*/
                $scope.editSubsciption = function (val) {
                    $scope.subscription = val;
                    $scope.isSubscriAddEdit = true;
                };
                
                /*-----------------Create Subscription-----------------*/
                $scope.saveSubscription = function (form) {
                    $scope.sub = true;
                    if (form.$valid) {
                        if($scope.subscription._id==null){
                              Subscription.create($scope.subscription).$promise.then(function (response) {
                                 $scope.sub = false;
                                $scope.getSubscriptionList();
                                $scope.isSubscriAddEdit = false;
                                   toastr.success("Subscription Added", "Successfully!");
                            }, function (error) {
                                 toastr.error("Something went wrong", "Error!");
                               $scope.errors.other = error.message;
                            });
                      }
                    else{
                            Subscription.update($scope.subscription).$promise.then(function (response) {
                                 $scope.sub = false;
                                $scope.getSubscriptionList();
                                toastr.success("Subscription Updated", "Successfully!");
                                $scope.isSubscriAddEdit = false;
                            }, function (error) {
                                   toastr.error("Something went wrong", "Error!");
                               $scope.errors.other = error.message;
                            });
                    }
                    }
                };

                 /*-----------------Delete Subscription-----------------*/
                $scope.deleteSubscription = function (subscription) {
                    Subscription.delete({id: subscription._id}).$promise.then(function (response) {
                                toastr.success("Subscription Deleted", "Successfully!");
                       $scope.getSubscriptionList();
                    }, function (error) {
                           toastr.error("Something went wrong", "Error!");
                        console.log(error);
                    });
                   };

                $scope.onLoad = function (fileObj) {
                    //   console.log("fileObj" +JSON.stringify(fileObj));
                };
                $scope.init();
                $scope.files = [];

            })

})();

