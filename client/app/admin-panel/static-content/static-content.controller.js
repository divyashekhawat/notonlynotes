(function () {
    'use strict';
    angular
            .module('contentManagement', ['ngQuill'])
            .config(['ngQuillConfigProvider', function (ngQuillConfigProvider) {
                    ngQuillConfigProvider.set({
                        toolbar: ['bold', 'italic', 'underline', 'strike', 'blockquote', 'link', 'image', {'size': ['small', false, 'large', 'huge']}, {'header': [1, 2, 3, 4, 5, 6, false]}, {'script': 'sub'}, {'script': 'super'}, {'list': 'ordered'}, {'list': 'bullet'}, {'color': []}, {'background': []}, {'align': []}, 'code-block']
                    }, 'snow', 'custom placeholder', ['bold', 'italic', 'underline', 'strike', 'blockquote', 'link', 'image', 'size', 'header', 'script', 'list', 'color', 'background', 'align', 'code-block']);

                }])

            .controller('contentManagementCtrl', function ($scope, StaticContentService, toastr) {
                $scope.showHideAddPage = false;
                  
                $scope.pageInit = function () {
                   getContent();
                    };

                 /*---------------Get Static content---------------------*/
                function getContent() {
                    StaticContentService.getAll().$promise.then(function (res) {
                       $scope.ContentPages = res;
                      console.log(res);
                      }, function (error) {
                        console.log(error);
                    });
                };
                
                $scope.addPage = function () {
                    $scope.showHideAddPage = true;
                    $scope.sub = false;
                      $scope.page = {};
             
                };
                
                $scope.editContent = function(data){
                  $scope.showHideAddPage = true;
                    $scope.sub = false;
                      $scope.page = data;  
                };
                
                $scope.saveContent = function (form) {
                    $scope.sub = true;
                    if (form.$valid) {
                        if ($scope.page._id) {
                            StaticContentService.update($scope.page).$promise.then(function (res) {
                                toastr.success("Content Updated", "Successfully!");
                                 $scope.showHideAddPage = false;
                            }, function (error) {
                                toastr.error("Something went wrong", "Error!");
                                console.log(error);
                            });
                        } else {
                                StaticContentService.create({content: $scope.page.content, type: $scope.page.type}).$promise.then(function (res) {
                                toastr.success("Content Added", "Successfully!");
                                 getContent();
                                  $scope.showHideAddPage = false;
                            }, function (error) {
                                console.log(error);
                                toastr.error("Something went wrong", "Error!");
                            });
                        }
                    }
                };
                
                $scope.deleteContent =function(data){
                      StaticContentService.delete({id:data._id}).$promise.then(function (res) {
                                toastr.success("Content Deleted", "Successfully!");
                                getContent();
                            }, function (error) {
                                toastr.error("Something went wrong", "Error!");
                                console.log(error);
                            });
                };
                 $scope.pageInit()
            });

})();

