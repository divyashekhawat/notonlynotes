(function () {
    'use strict';
    angular
            .module('dashboard', [])

            .controller('dashboardCtrl', function ($scope, Users, $state, $rootScope, journalCommonService) {
                $rootScope.manageSubAdminPage = '';
                $scope.sortType = 'username'; // set the default sort type
                $scope.sortReverse = false;  // set the default sort order
                $scope.searchUser = '';     // set the default search/filter term
                $scope.init = function () {
                    $scope.getUsers();
                };

                $scope.getUsers = function () {
                    Users.getUsersData(function (res) {
                        $scope.users = res;
                        $scope.totalUsers = $scope.users.length;
                        $scope.activeUsers = 0;
                        $scope.inActiveUsers = 0;
                        for (var i = 0; i < $scope.users.length; i++) {
                            if ($scope.users[i].active)
                                $scope.activeUsers++;
                            else
                                $scope.inActiveUsers++;
                        }
                    });
                };

                $scope.manageSubAdmin = function () {
                    $state.go('admin.subadmin', {page: 'manageadmin'});
                };

                $scope.addSubAdmin = function () {
                    $state.go('admin.subadmin', {page: 'subadmin'});
                };

                $scope.init();

            })

})();
