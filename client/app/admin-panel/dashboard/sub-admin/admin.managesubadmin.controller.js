(function () {
    'use strict';
    angular
            .module('subadmin', ['checklist-model'])

            .controller('manageSubAdminCtrl', function ($scope, $loading, Auth, $stateParams, Users, $state) {
                $scope.init = function () {
                    if ($stateParams.page == 'manageadmin')
                    {
                        $scope.subAdminPage = 'list';
                        $scope.getAdmin();
                    } else if ($stateParams.page == 'subadmin') {
                        $scope.subAdminPage = 'addEdit';
                        $scope.subadmin = {
                            first_name: '',
                            last_name: '',
                            username: '',
                            email: '',
                            password: '',
                            role: '',
                            accessPermission: ''
                        };
                    }
                };

                $scope.getAdmin = function () {
                    Users.getUsersData(function (res) {
                        $scope.users = res;
                        $scope.subAdmins = [];
                        for (var i = 0; i < $scope.users.length; i++) {
                            if ($scope.users[i].role == 'admin')
                                $scope.subAdmins.push($scope.users[i]);

                        }
                    });
                };

                $scope.accessPermision = [{
                        id: 1,
                        name: "Access Permission 1"
                    },
                    {
                        id: 2,
                        name: "Access Permission 2"
                    },
                    {
                        id: 3,
                        name: "Access Permission 3"
                    },
                    {
                        id: 4,
                        name: "Access Permission 4"
                    },
                    {
                        id: 5,
                        name: "Access Permission 5"
                    }
                ];
                
                /*---------------Sub Admin Listing----------------*/
                $scope.viewSubAdmin = function (val) {
                    $scope.subAdminPage = 'view';
                    $scope.subAdminData = val;
                    if ($scope.subAdminData.accessPermission.length > 0) {
                        $scope.accessPermisionList = [];
                        for (var i = 0; i < $scope.subAdminData.accessPermission.length; i++) {
                            for (var j = 0; j < $scope.accessPermision.length; j++) {
                                if ($scope.accessPermision[j].id == $scope.subAdminData.accessPermission[i])
                                    $scope.accessPermisionList.push($scope.accessPermision[j]);
                            }
                        }
                    }
                };

                $scope.editSubAdmin = function (val) {
                    $scope.subadmin = val;
                    $scope.subAdminPage = 'addEdit';
                };

                /*-----------------Create and update Sub admin--------------------*/
                $scope.saveSubAdmin = function (form) {
                    $scope.submitted = true;
                    if (form.$valid) {
                        $loading.start('adminsignup_loading');
                        if (!$scope.subadmin._id) {
                            $scope.subadmin.role = 'admin';
                            Auth.createUser($scope.subadmin)
                                    .then(function () {
                                        $loading.finish('adminsignup_loading');
                                        $scope.submitted = false;
                                        $scope.subAdminPage = 'list';
                                        $scope.getAdmin();
                                        $scope.subadmin = {
                                            first_name: '',
                                            last_name: '',
                                            username: '',
                                            email: '',
                                            password: '',
                                            role: '',
                                            accessPermission: ''
                                        };
                                    })
                                    .catch(function (err) {
                                        err = err.data;
                                        $scope.errors = {};
                                        $loading.finish('adminsignup_loading');
                                        /*********************Update validity of form fields that match the mongoose errors ******************************/

                                        if (err) {
                                            angular.forEach(err.errors, function (error, field) {
                                                form[field].$setValidity('mongoose', false);
                                                $scope.errors[field] = error.message;
                                            });
                                        }
                                    });
                        } else {
                            Auth.updateUser($scope.subadmin).then(function () {
                                $loading.finish('adminsignup_loading');
                                $scope.subAdminPage = 'list';
                                $scope.getAdmin();
                            })
                                    .catch(function (err) {
                                        err = err.data;
                                        $scope.errors = {};
                                        $loading.finish('adminsignup_loading');
                                        if (err) {
                                            angular.forEach(err.errors, function (error, field) {
                                                form[field].$setValidity('mongoose', false);
                                                $scope.errors[field] = error.message;
                                            });
                                        }
                                    })
                        }

                    }
                };
                $scope.init();
            })
})();
