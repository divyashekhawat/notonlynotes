(function () {
    'use strict';
    angular
            .module('notification', [])

            .controller('notificationCtrl', function ($scope, Notification) {
                $scope.init = function () {
                    $scope.getNotificationList();
                };

                  $scope.getNotificationList = function () {
                      Notification.get().$promise.then(function (response) {
                       $scope.notificationList = response.data;
                    }, function (error) {
                        console.log(error);
                    });
                  }

                $scope.deleteNotification = function (value) {
                    Notification.delete({id:value._id}).$promise.then(function (response) {
                       $scope.getNotificationList();
                    }, function (error) {
                        console.log(error);
                    });
                }

                $scope.init();

            })

})();

