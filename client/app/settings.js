'use strict';
(function() {
angular.module('Journalism')
.constant('Settings', {
  demo: false,
  menu: {
    auth : [
      {text:'login', icon: 'login', url: 'login'},
      {text:'signup', icon: 'login', url: 'signup'}
    ],
    admin : [
      {text:'Change Password', icon: 'settings', url: 'settings'},
      {text:'logout', icon: 'logout', url: 'logout'}
    ]
  }
});
})();
