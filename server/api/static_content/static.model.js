
'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var StaticSchema = new Schema({
    type: {type:String, enum:['about_us','terms','privacy_policy']},
    content: String,
}, {timestamps:true});

module.exports = mongoose.model('Static', StaticSchema);
