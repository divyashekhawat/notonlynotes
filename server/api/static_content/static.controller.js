'use strict';

var _ = require('lodash');
var fs = require('fs-extra');
var fsPath = require('fs-path');
var randtoken = require('rand-token');
var Static = require('./static.model');
var async = require('async');

function handleError(res, err) {
    return res.status(500).send(err);
}

// Get All of Static content
exports.index = function (req, res) {
    Static.find({}, function (err, content) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(200).json(content);
    });
};

// Get a single static content
exports.show = function (req, res) {
    Static.findOne({type: req.params.type}, function (err, content) {
        if (err)
            return handleError(res, err);
        return res.json(content);
    });
};

// Creates a new static content in the DB.
exports.create = function (req, res) {
    var domain = req.headers.host;
    var content = req.body
    Static.create(content, function (err, data) {
        if (err)
            return handleError(res, err);
        else {
            res.status(200).send({message: content.type + "added!"});
        }
    });
};

//// Updates an existing static in the DB.

exports.update = function (req, res) {
    var content = req.body;
    if (content._id) {
        delete content._id;
    }

    Static.findById(req.params.id, function (err, data) {
        if (err) {
            return handleError(res, err);
        }
        if (!data) {
            return res.status(404).send('Not Found');
        }
        var updated = _.merge(data, content);
        updated.save(function (save_err) {
            if (save_err) {
                return handleError(res, err);
            }
            res.status(200).json(updated)
        });
    });
};

/************************************** Remove Content  ********************************/

exports.delete = function(req, res) {
    Static.findByIdAndRemove(req.params.id).exec(function(err,result){
        if(err) return res.status(500).send({error:err,data:null,message:"Something went wrong"});
        else res.status(200).json({error:null,data:result,message:"Content deleted sucessfully"});
    })
}; 
