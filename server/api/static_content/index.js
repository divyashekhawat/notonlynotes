'use strict';

var express = require('express');
var controller = require('./static.controller');
var auth = require('../../auth/auth.service'); 

var router = express.Router();

router.get('/', controller.index);
router.get('/:type', controller.show);
router.post('/', auth.hasRole('admin'), auth.isAuthenticated(), controller.create);
router.put('/:id', auth.hasRole('admin'), auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.hasRole('admin'), auth.isAuthenticated(), controller.delete);

module.exports = router;