var mongoose = require('mongoose');

var subscription = mongoose.Schema({   
                
            imageSource	:{type:String,default:null},
            name :{type:String,default:null},
            description	:{type:String,default:null},
            price :{type:Number,default:null},
            validity :{type:Number,default:null},

},{timestamps:true});


module.exports = mongoose.model('subscription', subscription);
