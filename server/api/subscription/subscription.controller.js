'use strict';

var Subscription = require('./subscription.model');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var async = require('async');
var randtoken = require('rand-token')
var _ = require('lodash');
var nodemailer = require('nodemailer');
var auth = require('../../auth/auth.service');

var validationError = function(res, err) {
    return res.status(422).json(err);
};

/************************************** add Subscription by admin  ********************************/

exports.add = function(req, res) {
    
    req.body.price=+(req.body.price);
    
    var newSubscription = new Subscription(req.body);
    
    newSubscription.save(function(err,subscription){
        if(err) return res.status(500).send({error:err,data:null,message:"Subscription not added ! Something went wrong"});
        else res.status(200).json({error:null,data:subscription,message:"Subscription added sucessfully"});
    })
}; 

/************************************** update Subscription by admin  ********************************/

exports.update = function(req, res) {
 
    req.body.price=+(req.body.price);
   Subscription.update({_id:req.params.id},{$set:req.body},function(err,subscription){
        if(err) return res.status(500).send({error:err,data:null,message:"Subscription not updated ! Something went wrong"});
        else res.status(200).json({error:null,data:subscription,message:"Subscription updated sucessfully"});
    })
}; 

/************************************** get all Subscriptions  ********************************/

exports.getAllSubscriptions = function(req, res) {
    Subscription.find().exec(function(err,subscriptions){
        if(err) return res.status(500).send({error:err,data:null,message:"Something went wrong"});
        else res.status(200).json({error:null,data:subscriptions,message:"successfull"});
    })
}; 

/************************************** get one Subscriptions  ********************************/

exports.getOneSubscription = function(req, res) {
    Subscription.find({_id:req.params.id}).exec(function(err,subscription){
        if(err) return res.status(500).send({error:err,data:null,message:"Something went wrong"});
        else res.status(200).json({error:null,data:subscription,message:"sucessfull"});
    })
}; 

/************************************** Remove one Subscription  ********************************/

exports.delete = function(req, res) {
    Subscription.findByIdAndRemove(req.params.id).exec(function(err,subscription){
        if(err) return res.status(500).send({error:err,data:null,message:"Something went wrong"});
        else res.status(200).json({error:null,data:subscription,message:"Subscription deleted sucessfully"});
    })
}; 

/************************************** Subscription Controller Ends **************************************************/

