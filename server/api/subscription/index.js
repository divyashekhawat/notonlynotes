'use strict';

var express = require('express');
var controller = require('./subscription.controller');
var config = require('../../config/environment');


var router = express.Router();

router.post('/', controller.add);
router.get('/all', controller.getAllSubscriptions);
router.get('/:id', controller.getOneSubscription);
router.put('/:id', controller.update);
router.delete('/:id', controller.delete);

module.exports = router;


//         /api/subscription/             (POST)
//         /api/subscription/all          (GET)
//         /api/subscription/:id          (GET)
//         /api/subscription/:id          (PUT)
//         /api/subscription/:id          (DELETE)

