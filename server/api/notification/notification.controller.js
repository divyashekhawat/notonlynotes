'use strict';

var Notification = require('./notification.model');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var async = require('async');
var randtoken = require('rand-token')
var _ = require('lodash');
var nodemailer = require('nodemailer');
var auth = require('../../auth/auth.service');

var validationError = function(res, err) {
    return res.status(422).json(err);
};

/************************************** add Notification ********************************/

exports.add = function(req, res) {
    var newNotification = new Notification(req.body);
    newNotification.save(function(err,notification){
        if(err) return res.status(500).send({error:err,data:null,message:"Notification not added ! Something went wrong"});
        else res.status(200).json({error:null,data:notification,message:"Notification added sucessfully"});
    })
}; 

/************************************** update Notification  ********************************/

exports.update = function(req, res) {
 
    Notification.update({status:'unread'},{$set:{status:'read'}}, {multi:true},function(err,notification){
        if(err) return res.status(500).send({error:err,data:null,message:"Notification not updated ! Something went wrong"});
        else res.status(200).json({error:null,data:notification,message:"Notification updated sucessfully"});
    })
}; 

/************************************** get all Notification  ********************************/

exports.getAllNotifications = function(req, res) {
    Notification.find().exec(function(err,notification){
        if(err) return res.status(500).send({error:err,data:null,message:"Something went wrong"});
        else res.status(200).json({error:null,data:notification,message:"successfull"});
    })
}; 

/************************************** Remove one Notification  ********************************/

exports.delete = function(req, res) {
    Notification.findByIdAndRemove(req.params.id).exec(function(err,notification){
        if(err) return res.status(500).send({error:err,data:null,message:"Something went wrong"});
        else res.status(200).json({error:null,data:notification,message:"Notification deleted sucessfully"});
    })
}; 

/************************************** Notification Controller Ends **************************************************/

