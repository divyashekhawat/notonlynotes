var mongoose = require('mongoose');

var notification = mongoose.Schema({   
            message :{type:String,default:null},
            type:{type:String, enum:['newuser', 'other'], default: 'other'},
           status:{type:String,enum:['unread','read'],default:'unread'},
},{timestamps:true});


module.exports = mongoose.model('notification', notification);
