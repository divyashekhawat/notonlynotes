'use strict';

var express = require('express');
var controller = require('./notification.controller');
var config = require('../../config/environment');


var router = express.Router();

router.post('/', controller.add);
router.get('/all', controller.getAllNotifications);
router.put('/', controller.update);
router.delete('/:id', controller.delete);
module.exports = router;
