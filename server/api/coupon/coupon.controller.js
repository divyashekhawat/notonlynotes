'use strict';

var Coupon = require('./coupon.model');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var async = require('async');
var randtoken = require('rand-token')
var _ = require('lodash');
var nodemailer = require('nodemailer');
var auth = require('../../auth/auth.service');

var validationError = function(res, err) {
    return res.status(422).json(err);
};

/************************************** add Coupon by admin  ********************************/

exports.add = function(req, res) {
    
     var newCoupon = new Coupon(req.body);
    
    newCoupon.save(function(err,coupon){
        if(err) return res.status(500).send({error:err,data:null,message:"Coupon not added ! Something went wrong"});
        else res.status(200).json({error:null,data:coupon,message:"Coupon added sucessfully"});
    })
}; 

/************************************** update Coupon  ********************************/

exports.update = function(req, res) {
 
   Coupon.update({_id:req.params.id},{$set:req.body},function(err,coupon){
        if(err) return res.status(500).send({error:err,data:null,message:"Coupon not updated ! Something went wrong"});
        else res.status(200).json({error:null,data:coupon,message:"Coupon updated sucessfully"});
    })
}; 

/************************************** get all Coupons  ********************************/

exports.getAllCoupons = function(req, res) {
    Coupon.find().exec(function(err,coupon){
        if(err) return res.status(500).send({error:err,data:null,message:"Something went wrong"});
        else res.status(200).json({error:null,data:coupon,message:"successfull"});
    })
}; 

/************************************** Remove Coupons  ********************************/
exports.delete = function(req, res) {
    Coupon.findByIdAndRemove(req.params.id).exec(function(err,coupon){
        if(err) return res.status(500).send({error:err,data:null,message:"Something went wrong"});
        else res.status(200).json({error:null,data:coupon,message:"Coupon deleted sucessfully"});
    })
}; 


/************************************** Coupon Controller Ends **************************************************/

