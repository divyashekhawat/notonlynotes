var mongoose = require('mongoose');

var coupon  = mongoose.Schema({   
            imageSource	:{type:String,default:null},
            name :{type:String,default:null},
            description	:{type:String,default:null},
            status:{type:Boolean, default:false},
            startDate:Date,
            endDate:Date,
            noOfUser:{type:Number, default:0},
            noOfCouponUsedUsers:{type:Number, default:0},
            discount:{type:Number, default:0}

},{timestamps:true});


module.exports = mongoose.model('coupon', coupon);



