'use strict';

var express = require('express');
var controller = require('./coupon.controller');
var config = require('../../config/environment');


var router = express.Router();

router.post('/', controller.add);
router.get('/all', controller.getAllCoupons);
router.put('/:id', controller.update);
router.delete('/:id', controller.delete);
module.exports = router;


