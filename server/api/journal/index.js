'use strict';

var express = require('express');
var controller = require('./journal.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

 router.post('/', auth.isAuthenticated(), controller.addJournal);
 router.post('/one',auth.isAuthenticated(),controller.getOneJournal); 
 router.put('/:id',auth.isAuthenticated(),controller.updateJournal);

module.exports = router;
