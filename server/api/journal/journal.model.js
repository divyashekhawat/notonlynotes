var mongoose = require('mongoose');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
 
var schema = new mongoose.Schema({
       
       userId:{type:ObjectId,ref:'user'},
       journalDate:Number,
       journalMonth:Number,//0-12
       journalYear:Number,       
       journalContent:{type:String,default:null}

},{timestamps:true});

schema.index({ journalDate: 1, journalMonth: 1 ,journalYear : 1 , userId:1},{unique: true});

module.exports=mongoose.model('journal',schema);