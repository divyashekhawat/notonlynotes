'use strict';

var Journal = require('./journal.model');
var core = require('./../core/core.controller');


/**********************************************************************
 * API -> add new journal
 * METHOD -> POST
 * URL ->api/journal
 * Response -> new document
 * request -> {journalContent: content,journalAddDate:date}
 *      
 *********************************************************************/

exports.addJournal = function (req, res) {

    var currentDate = new Date(req.body.journalAddDate);

    var journal = new Journal({

       userId:req.user._id,
       journalDate:currentDate.getDate(),
       journalMonth:currentDate.getMonth(),
       journalYear:currentDate.getFullYear(),      
       journalContent:req.body.journalContent||''

    });

    journal.save(function(err,journal){
        if (err) return core.handleError(err,res);
        else res.status(200).json(journal);
    });

}

/**********************************************************************
 * API -> update new journal
 * METHOD -> PUT
 * URL ->api/journal/:journalId
 * Response -> updated document
 * request -> {journalContent: content}
 *      
 *********************************************************************/

exports.updateJournal = function (req, res) {
    var id = req.params.id;

    var dataToUpdate = {
        journalContent: req.body.journalContent||''
    };

    Journal.findByIdAndUpdate(id,{$set:dataToUpdate},{new:true})
        .lean()
        .exec(function (err, journal) {
            if (err) return core.handleError(err,res);
            else res.status(200).json({data:journal});
    })
};

/**********************************************************************
 * API -> get single journal
 * METHOD -> POST
 * URL ->api/journal/one
 * Response -> single document
 * request -> {journalAddDate:date}
 *      
 *********************************************************************/
exports.getOneJournal = function (req, res) {

    var journalDate = new Date(req.body.journalAddDate);
    
    var where = {
       userId:req.user._id,
       journalDate:journalDate.getDate(),
       journalMonth:journalDate.getMonth(),
       journalYear:journalDate.getFullYear()       
    };

    Journal.findOne(where)
        .lean()
        .exec(function (err, journal) {
            if (err) return core.handleError(err,res);
            else res.status(200).json({data:journal});
        })
};

/**********************************************************************
                             end 
 *********************************************************************/