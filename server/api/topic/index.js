'use strict';

var express = require('express');
var CNTLR = require('./topic.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/:date',auth.isAuthenticated(),CNTLR.getActiveTopics);
router.post('/',auth.isAuthenticated(),CNTLR.createTopic);
router.put('/:topicId',auth.isAuthenticated(),CNTLR.updateTopic);
router.put('/entity/:topicId',auth.isAuthenticated(),CNTLR.pushNewEntity);
router.delete('/:topicId',auth.isAuthenticated(),CNTLR.deleteTopic);

module.exports = router;
