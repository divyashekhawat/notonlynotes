var mongoose = require('mongoose');

var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;
    

var topic = new mongoose.Schema({
       userId:{type:ObjectId,ref:'user'},
       name : String,
       entities : [
           {
               content : String,
               at : Date,               
           }
       ],
       goals : {
                isSet:Boolean,
                wordCount : Number,
                type:{type:String,enum:['entry','day','week','month','year'],default:'entry'},
                finishAt : Date
       },
       setting : {
                isSet:Boolean,
                overAllWordsStatus:Boolean,
                overAllWordCount : Number,
                finishAt:Date
       },
       Status:{type:String,enum:['inactive','active',],default:'active'}       
       
},{timestamps:true});

module.exports=mongoose.model('topic',topic);