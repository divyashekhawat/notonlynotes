'use strict';

var Topic = require('./topic.model');
var core = require('./../core/core.controller');
var moment = require('moment');

/**********************************************************************
 * API -> create new topic
 * METHOD -> POST
 * URL ->api/topic
 * Response -> new document
 * request -> {name:topicName , goals:{isSet:true,wordCount:100,type:'daily',finishAt:date} or null ,setting:{isSet:true,overAllWordCount,finishAt:date} or null }
 *      
 *********************************************************************/

exports.createTopic = function(req,res){

    var d = req.body;   

    var goals = { isSet: d.goals.isSet, wordCount: d.goals.wordCount || 100000, type: d.goals.type, finishAt: d.goals.finishAt };
    var setting = { isSet: d.setting.isSet, overAllWordCount: d.setting.overAllWordCount || 100000, finishAt: d.setting.finishAt };

    var topic = new Topic({
        userId: req.user._id || null,
        name: d.name || '',
        entities: [],
        goals: goals,
        setting: setting
    })    

    topic.save(function(err,topic){
        if(err) return core.handleError(err,res);
        else res.status(200).json(topic);
    });
}

/**********************************************************************
 * API -> get Topic List
 * METHOD -> GET
 * URL ->api/topic/:'date'
 * Response -> topics
 * 
 *********************************************************************/

exports.getActiveTopics = function(req,res){

    var userId = req.user._id || null ;
    var date = req.params.date ;

    var yesterday = moment(date).add(0, 'days');
    var tommorow =  moment(date).add(1, 'days');
   
    Topic.find({userId : userId , Status:'active' ,"createdAt": {"$gte":yesterday,"$lt":tommorow}})
        .sort("-createdAt")
        .lean()
        .exec(function(err,topics){
            if(err) return core.handleError(err,res);
            else res.status(200).json({data:topics});
        });

}

/**********************************************************************
 * API -> get Topic List
 * METHOD -> PUT
 * URL ->api/topic/topicId
 * Response -> topics
 * Request ->{name:topicName , goals:{isSet:true,wordCount:100,type:'daily',finishAt:date} or null ,setting:{isSet:true,overAllWordCount,finishAt:date} or null }
 * 
 *********************************************************************/

exports.updateTopic = function(req,res){

    var id = req.params.topicId || null ;
    var userId = req.user._id || null ;

    var d = req.body;

  var goals = { isSet: d.goals.isSet, wordCount: d.goals.wordCount || 100000, type: d.goals.type, finishAt: d.goals.finishAt };
    var setting = { isSet: d.setting.isSet, overAllWordCount: d.setting.overAllWordCount || 100000, finishAt: d.setting.finishAt };

    var dataToUpdate = {
        name: d.name,     
        goals: goals,
        setting: setting
    }    

    Topic.findOneAndUpdate({_id:id,userId:userId},{$set:dataToUpdate})
        .lean()
        .exec(function(err,topic){
            if(err) return core.handleError(err,res);
            else res.status(200).json(topic);
        })
}

/**********************************************************************
 * API -> delete topic 
 * METHOD -> Delete
 * URL ->api/topic/topicId
 * Response -> done
 * 
 *********************************************************************/

exports.deleteTopic = function (req,res){

    var id = req.params.topicId || null ;
    var userId = req.user._id || null ;

    Topic.findOneAndRemove({_id:id,userId:userId})
        .lean()
        .exec(function(err){
            if(err) return core.handleError(err,res);
            else res.status(200).json({done:1});
        })

}

/**********************************************************************
 * API -> push entity in topic
 * METHOD -> put
 * URL ->api/topic/entity/topicId
 * Response -> done
 * Request -> {content:''}
 *********************************************************************/

exports.pushNewEntity = function (req,res){

    var id = req.params.topicId || null ;
    var userId = req.user._id || null ;

    var newEntity = {
        content:req.body.content,
        at:new Date()
    };

    Topic.findOneAndUpdate({_id:id,userId:userId},{$push:{'entities': newEntity }})
        .lean()
        .exec(function(err,doc){
            if(err) return core.handleError(err,res);
            else res.status(200).json({data:doc});
        })

}

/**********************************************************************
 *               End
 *********************************************************************/

//create topic
//topic get 
//topic edit 
//topic delete 
// entity push