'use strict';

var User = require('./user.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var async = require('async');
var randtoken = require('rand-token')
var _ = require('lodash');
var nodemailer = require('nodemailer');
var auth = require('../../auth/auth.service');

var validationError = function(res, err) {
    return res.status(422).json(err);
};

/************************************** Get list of users *restriction: 'admin' ********************************/

exports.index = function(req, res) {
    User.find({}, '-salt -hashedPassword', function(err, users) {
        if (err) return res.status(500).send(err);
        res.status(200).json({error:null,data:users,message:"sucessfull"});
      });
}; 

/************************************************ Creates a new user ********************************************/

exports.create = function(req, res, next) {
    var newUser = new User(req.body);
    newUser.provider = 'local';
    newUser.save(function(err, user) {
        if (err)
            return validationError(res, err);
        async.waterfall([
            function(done) {
                var token = randtoken.generate(16);
                done(null, token);
            },
            function(token, done) {
                User.findOne({ $or: [ { email: user.email }, { email: user.username } ] }, function(err, userdata) {
                    if (userdata) {
                        userdata.newUserToken = token;
                        userdata.save(function(err, emailData) {
                            if (!err) {
                                done(token, userdata);
                            } else {
                                return res.json({
                                    authentication: false,
                                    message: 'Please try again later'
                                })
                            }
                        });
                    } else {
                        return res.json({
                            authentication: false,
                            message: 'No account with the entered email address  or username exists.'
                        })
                    }
                });
            }
        ], function(token, newUser, done) {
            var domain = req.headers.host;
            var mailOptions = {
                to: newUser.email,
                from: 'Journalism-NotOnlyNotes <' + config.configMail.email + '>',
                subject: 'Email To your registration request.',
                text: 'Click on below link to verify your email address for successfull registration' +
                    '  http://' + domain + '/api/users/mail/verify_email?token=' + token + '\n\n' +
                    'thankyou.'
            };
            var transporter = nodemailer.createTransport({
                service: "Gmail",
                auth: {
                    user: config.configMail.email,
                    pass: config.configMail.password
                }
            });
            transporter.sendMail(mailOptions, function(err) {
                if (err) {
                    return res.json({
                        authentication: false,
                        message: 'Sorry !!! We are at present unable to send mail please try again later'
                    })
                } else {
                    var tokens = auth.signToken(user._id, user.role);
                    res.json({ token: tokens });
                }

            });
        });
    });
};

/************************************** User Verification email **************************************************/

exports.verify_email = function(req, res){
   var data = req.query;
    var timeStamp = Date.now();
    if (data.token) {
        User.find({ newUserToken: data.token }, function(err, user) {
           if (user.length > 0) {
                    user[0].active = true;
                    user[0].save(function(err) {
                        if (err) return validationError(res, err);
                       res.status(200).send('Hey its verified successfully!! Bravo ..!!');
                    });
            } else {
                res.status(204).send('plesae go away');
            }
        });
    } else {
        res.status(204).send('This is Not a vaild url.');
    }

}

/**************************************************Get a single user*********************************************/
exports.show = function(req, res, next) {
    var userId = req.params.id;
    User.findById(userId, function(err, user) {
        if (err) return next(err);
        if (!user) return res.status(401).send('This is an Unauthorized Access');
        res.json(user.profile);
    });
};

/**********************************************Edit user profile**************************************************/


exports.update = function(req, res) {
    var userId = req.params.id;
   req.body.updated = Date.now();
    User.findById(userId, function(err, user) {
        if (err) {
            return handleError(res, err); }
        if (!user) {
            return res.status(401).send('This is an Unauthorized Access'); }
        var updated = _.merge(user, req.body);
        updated.save(function(err) {
            if (err) {
                return handleError(res, err); }
            return res.status(200).json(user.profile);
        });
    });
};


/**************************************Deletes a user * restriction: 'admin'*************************************/
exports.destroy = function(req, res) {
    User.findByIdAndRemove(req.params.id, function(err, user) {
        if (err) return res.status(500).send(err);
        return res.status(204).send('No Content to display');
    });
};

/**************************************Change a user password **************************************************/
exports.changePassword = function(req, res, next) {
    
    var userId = req.user._id;
    var oldPass = String(req.body.oldPassword);
    var newPass = String(req.body.newPassword);
    User.findById(userId, function(err, user) {
        if (user.authenticate(oldPass)) {
            user.password = newPass;
            user.save(function(err) {
                if (err) return validationError(res, err);
                res.status(200).send('OK');
            });
        } else {
            res.status(403).send('Its a Forbidden request');
        }
    });
};
/************************************** User Forgot Password **************************************************/

exports.forgotPassword = function(req, res) {
    async.waterfall([
        function(done) {
            var token = randtoken.generate(16);
            done(null, token);
        },
        function(token, done) {
            User.findOne({ email: req.body.email }, function(err, user) {
                if (user) {
                    user.resetPasswordToken = token;
                    user.resetPasswordExpires = Date.now() + 3600000;
                    user.save(function(err, userData) {
                       if (!err) {
                            done(token, user);
                        } else {
                            return res.json({
                                authentication: false,
                                message: 'Please try again later. Thak-you !!'
                            })
                        }
                    });
                } else {
                    return res.json({
                        authentication: false,
                        message: 'No account with this email address exists.'
                    })
                }

            });
        }
    ], function(token, user, done) {
        var domain = req.headers.host;
        var mailOptions = {
            to: user.email,
            from: 'Journalism-NotOnlyNotes <' + config.configMail.email + '>',
            subject: 'Hey this is an Email To your Forgot Password Request',
            text: 'Click the below mentioned link to reset your password' +
                'http://' + domain + '/verify?token=' + token + '\n\n' +
                'thankyou.'
        };
        var transporter = nodemailer.createTransport({
            service: "Gmail",
            auth: {
                user: config.configMail.email,
                pass: config.configMail.password
            }
        });

        transporter.sendMail(mailOptions, function(err) {
            if (err) {
                return res.json({
                    authentication: false,
                    message: 'Sorry!! We are unable to send mail please try again later'
                })
            } else {
               res.json({
                    authentication: true,
                    message: 'Send message to ensure successfull password-reset'
                })
            }

        });
    });
}

/************************************** User Reset Password **************************************************/

exports.resetPassword = function(req, res) {
    var data = req.body;
    var timeStamp = Date.now();
    if (data.token) {
        User.find({ resetPasswordToken: data.token }, function(err, user) {
            if (user.length > 0) {
                if (timeStamp <= user[0].resetPasswordExpires) {
                    user[0].password = data.password;
                    user[0].resetPasswordExpires = 0;
                    user[0].save(function(err) {
                        if (err) return validationError(res, err);
                        res.status(200).send('You have successfully reseted your password!');
                    });
                } else {
                    res.status(204).send('link expired');
                }
            } else {
                res.status(204).send('link expired');
            }
        });
    } else {
        res.status(204).send('Not a valid url request');
    }
}

/***************************** Get My Information (User Profile) Particular user data **************************/

exports.me = function(req, res, next) {
    var userId = req.user._id;
    User.findOne({
        _id: userId
    }, '-salt -hashedPassword', function(err, user) { // don't give away the password or salt
        if (err) return next(err);
        if (!user) return res.status(401).send('Unauthorized Access');
        res.json(user);
    });
};

/************************************** Authentication Callback ***********************************************/
exports.authCallback = function(req, res, next) {
    res.redirect('/');
};

/************************************** User Controller Ends **************************************************/