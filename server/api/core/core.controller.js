'use strict';

exports.validationError = function (res, err) {
    return res.status(422).json(err);
};

exports.handleError = function (err,res){
    res.status(400).json({err:err});
    return;
}