'use strict';

var express = require('express');
var controller = require('./contactUs.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.post('/', controller.add);
router.put('/update/:id', controller.update);
router.get('/get/all',controller.getAllForms);

module.exports = router;
