'use strict';

var ContactUs = require('./contactUs.model');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var async = require('async');
var randtoken = require('rand-token')
var _ = require('lodash');
var nodemailer = require('nodemailer');
var auth = require('../../auth/auth.service');

var validationError = function(res, err) {
    return res.status(422).json(err);
};

/************************************** add mesage/query form by user  ********************************/

exports.add = function(req, res) {
    var contactUsForm = new ContactUs(req.body);
    contactUsForm.save(function(err,data){
        if(err) return res.status(500).send({
            error:err,
            data:null,
            message:"Form not added ! Something went wrong"
        });
        else res.status(200).json({error:null,data:data,message:"Form added sucessfully"});
    })
}; 

/************************************** update contactUs form by admin  ********************************/

exports.update = function(req, res) {
    req.params.id="58e24811c68a74274e670120";
   ContactUs.update({_id:req.params.id},{$set:{isUpdated:true}},function(err,data){
        if(err) return res.status(500).send({
            error:err,
            data:null,
            message:"Something went wrong"
        });
        else res.status(200).json({error:null,data:data,message:"Updated sucessfully"});
    })
}; 

/************************************** update contactUs form by admin  ********************************/

exports.getAllForms = function(req, res) {
   ContactUs.find().exec(function(err,data){
        if(err) return res.status(500).send({
            error:err,
            data:null,
            message:"Something went wrong"
        });
        else res.status(200).json({error:null,data:data,message:"sucessfull"});
    })
}; 


/************************************** User Controller Ends **************************************************/