var mongoose = require('mongoose');

var contactUs = mongoose.Schema({   

            firstName : {type:Number,default:null},
            lastName : {type:String,default:null},
            email : {type:String,default:null},
            message : {type:String,default:null},
            isUpdated: {type:Boolean,default:false}

},{timestamps:true});


module.exports = mongoose.model('contactUs', contactUs);
