/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');
var path = require('path');

module.exports = function(app) {

  // Insert routes below

  app.use('/api/users', require('./api/user'));                         // USER 
  
  app.use('/api/topic', require('./api/topic'));                         // TOPIC

  app.use('/api/subscription', require('./api/subscription'))           // SUBSCRIPTION

  app.use('/api/contactus', require('./api/contact_us'))                // CONTACT US

  app.use('/api/journal', require('./api/journal'))                     // JOURNAL
  
  app.use('/api/notification', require('./api/notification'))           // NOTIFICATION
  
  app.use('/api/coupon', require('./api/coupon'))                       // COUPON
  
   app.use('/api/content', require('./api/static_content'));            // Static Content
  
  app.use('/auth', require('./auth'));
  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets|client)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
};