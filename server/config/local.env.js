'use strict';

// Use local.env.js for environment variables that grunt sets when the server starts locally.

module.exports = {
  DOMAIN:           'http://localhost:7000',
  SESSION_SECRET:   'crud-secret',
  DEBUG: ''
};
