var gulp = require('gulp'),
        concat = require('gulp-concat'),
        clean = require('gulp-clean'),
        watch = require('gulp-watch');
gulp.task('cleanfile', function () {
    gulp.src(['assests/js/vendor.js']).pipe(clean({force: true}));
});
gulp.task('css', function () {
    return gulp.src([
        "client/assests/css/quill.snow.css",
        "client/assests/css/bootstrap.min.css",
        "client/assests/css/colorpicker.min.css",
        "client/assests/css/angular-bootstrap-calendar.min.css",
         "client/app/journal/css/theme-medium.css",
        "client/assests/css/font-awesome.min.css",
        "client/assests/css/slick.css",
        "client/assests/css/slick-theme.css",
        "client/assests/css/style.css",
        "client/assests/css/responsive.css",
        "client/bower_components/nya-bootstrap-select/dist/css/nya-bs-select.css"
    ])
            .pipe(concat('main.css'))
            .pipe(gulp.dest('client/assests/css/'));
});

gulp.task('vendor', function () {
    gulp.src([
        "client/bower_components/angular/angular.js",
         "client/bower_components/angular-animate/angular-animate.js",
        "client/bower_components/angular-bootstrap/ui-bootstrap-tpls.js",
        "client/assests/js/rrule.js",
        "client/bower_components/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.min.js",
        "client/bower_components/socket.io-client/dist/socket.io.js",
        "client/bower_components/angular-socket-io/socket.js",
        "client/assests/js/angular-bootstrap-calendar-tpls.js",
        "client/bower_components/angular-sanitize/angular-sanitize.js",
        "node_modules/oclazyload/dist/ocLazyLoad.js",
        "node_modules/angular-ui-router/release/angular-ui-router.js",
        "node_modules/angular-resource/angular-resource.js",
        "node_modules/angular-toastr/dist/angular-toastr.tpls.js",
        "client/bower_components/angular-cookies/angular-cookies.min.js",
       "client/app/journal/js/quill.js",
        "node_modules/ng-quill/src/ng-quill.js",
        "client/bower_components/nya-bootstrap-select/dist/js/nya-bs-select.min.js",
    ])
            .pipe(concat('vendor.js'))
            .pipe(gulp.dest('client/assests/js/'));
});

gulp.task('watch', function () {
    gulp.watch(['assests/css/*.css'], ['css']);
});
gulp.task('default', ['vendor', 'css', 'watch']);

